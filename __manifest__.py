# -*- coding: utf-8 -*-
{
    'name': "custom_products",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/flectra/flectra/blob/master/flectra/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'stock','product', 'sale_management', 'purchase', 'website_sale', 'sale', 'web', ],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'static/src/xml/static_load.xml',
        'views/sale_report_template.xml',
        'views/mondo_sale_report.xml',
        'views/reports.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/mail_template_data.xml',
        'views/res_config_settings_views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}