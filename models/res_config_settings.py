
from flectra import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    default_show_images = fields.Boolean(string="Show Images in Quotation Lines", default_model="sale.order", )
