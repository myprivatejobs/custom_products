# -*- coding: utf-8 -*-

from flectra import models, fields, api, _
from flectra.exceptions import Warning


class ProductTemplate(models.Model):
    _name = 'product.template'
    _inherit = 'product.template'

    pricelist_item_ids = fields.One2many(comodel_name="product.pricelist.item", inverse_name="product_tmpl_id",
                                         string="Price List", )

    ean_code = fields.Char(string="EAN", required=False)
    depth = fields.Float(string="Depth", required=False)
    height = fields.Float(string="Height", required=False)
    length = fields.Float(string="Length", required=False)
    top500 = fields.Boolean(string="Top 500")
    private_label = fields.Boolean(string="Private Label")
    flagship_product = fields.Boolean(string="Best Seller")
    intrastate_code = fields.Char("Intrastate Code")
    adr_onu = fields.Integer(string="ONU", )
    adr_class = fields.Integer(string="Class", )
    adr_description = fields.Text(string="Description", )
    adr_ge = fields.Char(string="GE", )
    adr_md_adr = fields.Boolean(string="MD ADR", )
    adr_limited_qty = fields.Boolean(string="Limited Qty", )
    adr_internal_packing = fields.Char(string="Internal Packing", required=False)
    adr_packaging = fields.Char(string="Packaging", required=False)
    unit_weight = fields.Float(string="Unit Weight", required=False)
    carton_weight = fields.Float(string="Carton Weight", required=False)
    package_weight = fields.Float(string="Package Weight", required=False)
    parcel_weight = fields.Float(string="Parcel Weight", required=False)
    internal_note = fields.Text(string="Internal Note", required=False)
    gamme_id = fields.Many2one(comodel_name="gamme.gamme", string="Gamme", required=False)
    etiquette_1 = fields.Integer(string="Etiquette 1")
    etiquette_2 = fields.Integer(string="Etiquette 2")
    etiquette_3 = fields.Integer(string="Etiquette 3")
    tunnel_code = fields.Char(string="Tunnel Code")
    uvc = fields.Integer(string="UVC", required=False)
    s_uvc = fields.Integer(string="S-UVC", required=False)
    barcode_unit = fields.Char(string="Barcode (Unit)", required=False, )
    barcode_packet = fields.Char(string="Barcode (Packet)", required=False, )
    barcode_pallet = fields.Char(string="Barcode (Pallet)", required=False, )
    security_sheet = fields.Char(string="Security Sheet", )
    technical_sheet = fields.Char(string="Technical Sheet", )


class Gamme(models.Model):
    _name = 'gamme.gamme'
    _rec_name = 'name'
    _description = 'Gamme'

    name = fields.Char(string="Gamme Name", required=False)


class ProductCategory(models.Model):
    _name = 'product.category'
    _inherit = 'product.category'

    description = fields.Text(string="Description", translate=True, )


class SaleOrderLine(models.Model):
    _name = 'sale.order.line'
    _inherit = 'sale.order.line'

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        # res = super(SaleOrderLine, self).product_id_change()
        # print(res)
        # return res
        if not self.product_id:
            return {'domain': {'product_uom': []}}

        vals = {}
        domain = {'product_uom': [('category_id', '=', self.product_id.uom_id.category_id.id)]}
        if not self.product_uom or (self.product_id.uom_id.id != self.product_uom.id):
            vals['product_uom'] = self.product_id.uom_id
            vals['product_uom_qty'] = 1.0

        product = self.product_id.with_context(
            lang=self.order_id.partner_id.lang,
            partner=self.order_id.partner_id.id,
            quantity=vals.get('product_uom_qty') or self.product_uom_qty,
            date=self.order_id.date_order,
            pricelist=self.order_id.pricelist_id.id,
            uom=self.product_uom.id
        )

        result = {'domain': domain}

        title = False
        message = False
        warning = {}
        if product.sale_line_warn != 'no-message':
            title = _("Warning for %s") % product.name
            message = product.sale_line_warn_msg
            warning['title'] = title
            warning['message'] = message
            result = {'warning': warning}
            if product.sale_line_warn == 'block':
                self.product_id = False
                return result

        name = product.name_get()[0][1]
        if ']' in name and '[' in name:
            name = name.split('] ')[1]
        if product.description_sale:
            name += '\n' + product.description_sale
        vals['name'] = name

        self._compute_tax_id()

        if self.order_id.pricelist_id and self.order_id.partner_id:
            vals['price_unit'] = self.env['account.tax']._fix_tax_included_price_company(self._get_display_price(product), product.taxes_id, self.tax_id, self.company_id)
        self.update(vals)

        return result


class SaleOrder(models.Model):
    _name = 'sale.order'
    _inherit = 'sale.order'

    show_images = fields.Boolean(string="Show Images in Quotation Lines", )
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('waiting_approval', 'Waiting Approval'),
        ('sale', 'Sales Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
        ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')

    @api.multi
    def write(self, values):
        for line in self.order_line:
            if not line.price_subtotal >= line.pv_min1 * line.product_uom_qty and self.state != 'waiting_approval':
                values.update({'state': 'waiting_approval'})
                break
        return super(SaleOrder, self).write(values)


class Users(models.Model):
    _name = 'res.users'
    _inherit = 'res.users'

    show_warning = fields.Boolean(string="Show / Hide Commission Warning")


class SaleOrderLine(models.Model):
    _name = 'sale.order.line'
    _inherit = 'sale.order.line'

    image_medium = fields.Binary(string="Image", related="product_id.image_medium", )
    show_images = fields.Boolean(string="Show Images", related="order_id.show_images", store=True, )
    pv_min1 = fields.Float(string="PV Min. 1", compute='_compute_pv_min_1_2', store=True, )
    pv_min2 = fields.Float(string="PV Min. 2", compute='_compute_pv_min_1_2', store=True, )
    commission = fields.Float(string="Commission", compute='_compute_commission', store=True, )
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('waiting_approval', 'Waiting Approval'),
        ('sale', 'Sales Order'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], related='order_id.state', string='Order Status', readonly=True, copy=False, store=True, default='draft')

    @api.multi
    @api.depends('price_subtotal', 'pv_min2', 'product_uom_qty')
    def _compute_commission(self):
        for rec in self:
            rec.commission = rec.price_subtotal - rec.pv_min2 * rec.product_uom_qty

    @api.multi
    @api.depends('product_id', 'order_id')
    def _compute_pv_min_1_2(self):
        for rec in self:
            pricelist_item_obj = rec.env['product.pricelist.item'].search([('product_tmpl_id', '=', rec.product_id.product_tmpl_id.id), ('pricelist_id', '=', rec.order_id.pricelist_id.id)])
            print(pricelist_item_obj)
            rec.pv_min1 = pricelist_item_obj and pricelist_item_obj.pv_min1 or 0.0
            rec.pv_min2 = pricelist_item_obj and pricelist_item_obj.pv_min2 or 0.0

    @api.onchange('price_unit', 'pv_min1', 'discount')
    def _check_price_unit_vs_pv_min1(self):
        if self.product_id:
            if not self.price_subtotal >= self.pv_min1 * self.product_uom_qty:
                return {
                    'warning': {
                        'title': "Low Price",
                        'message': _("The sales price is too low. The min price should be %s €. The offer/order will need a manager approval." % self.pv_min1),
                    },
                }

    @api.onchange('price_unit', 'pv_min2', 'discount')
    def _check_price_unit_vs_pv_min2(self):
        if self.product_id:
            if self.env.user.show_warning:
                if not self.price_subtotal > self.pv_min2 * self.product_uom_qty:
                    return {
                        'warning': {
                            'title': "Low Price",
                            'message': _(
                                "The sales price is too low. Your commission will be negative, you will loose %s €. The min sales price should be %s €." %(self.commission * -1, self.pv_min2)),
                        },
                    }
